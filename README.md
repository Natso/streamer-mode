# Streamer mode

Hides all mod tools on Lichess, so mods can stream safe and anonymously.
I put the following rules in Ublock Origin.

Add main to your dashboard of ublock origin. Everything is commented so feel free to remove if anything isn't needed for you.

## How to import

You can reach the dashboard by clicking on ublock origin > click the gears: ![The 'open the dashboard' button](https://user-images.githubusercontent.com/886325/102916299-9b30c000-4483-11eb-92de-d54d53674436.png)

Then go to 'my filters', add the code (remove anything not needed) and press 'apply changes'.

![Where the filters can be added](https://user-images.githubusercontent.com/585534/85202191-a0ddfb00-b2d2-11ea-8f93-a360c338ded7.png)

## Not full protection

The only things that people can recognise you for a mod are when you go to the account page of a closed account. I did hide the red closed sign, but it's not full protection. Also game analysis could dox you as the tabs are removed, although I doubt anyone except ijh would notice. The amount of notes could be another thing, but that's not anything major. With this script it's not possible to report messages in public chats. When you press 'm' it only rescales the page and when you search for a not-existing account it will show an empty page.
